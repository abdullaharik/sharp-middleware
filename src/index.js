const {
  promises,
  createReadStream,
  createWriteStream } = require('fs')
const path            = require('path')
const Sharp           = require('sharp')
// const request         = require('bent')



/**
 * Regular Expression Catch's
 *
 * /200x200/filename.jpg # basic with defaults
 * /200x200/filename.jpg.webp # make webp
 * /w200/filename.jpg # resize by width
 * /h200/filename.jpg # resize by height
 * /200x200a1/filename.jpg #not active
 * /200x200q1/filename.jpg #quality for jpeg
 */
const regex = s =>
  s.match(
    /\/((?:(?:(\d+)x(\d+))|(?:w(\d+))|(?:h(\d+)))(?:(?:q(\d+))?(?:a(\d+))?))\/(.*(gif|jpg|jpeg|tiff|png))(?:\.(webp))?/
  )
const getIntOrNull = x => parseInt(x, 10) || null

const getOptions = url => {
  // FULL SET: {full,folder,width,height,onlyWidth,onlyHeight,quality,action,fileName,fileExtension,isWebp};

  let [
    full,
    folder,
    width,
    height,
    onlyWidth,
    onlyHeight,
    quality,
    action,
    fileName,
    fileExtension,
    isWebp
  ] = regex(url)

  width = getIntOrNull(width)
  height = getIntOrNull(height)
  onlyWidth = getIntOrNull(onlyWidth)
  onlyHeight = getIntOrNull(onlyHeight)
  quality = getIntOrNull(quality)
  action = getIntOrNull(action)
  fileExtension = fileExtension ? fileExtension.toLowerCase() : fileExtension
  isWebp = isWebp ? true : false

  return {
    full,
    folder,
    width,
    height,
    onlyWidth,
    onlyHeight,
    quality,
    action,
    fileName,
    fileExtension,
    isWebp
  }
}


const getTransformer = ({
  width,
  height,
  onlyWidth,
  onlyHeight,
  overlayImg,
  gravity,
  fileExtension,
  quality,
  isWebp,
}) => {
  const sharp = Sharp()

  if (width && height) {
    sharp.resize(width, height)
  } else if (onlyWidth) {
    sharp.resize(onlyWidth)
  } else if (onlyHeight) {
    sharp.resize(null, onlyHeight)
  }

  if (fileExtension == 'jpg' || fileExtension == 'jpeg') {
    sharp.jpeg({ quality })
  }
  if (fileExtension == 'png') {
    sharp.png({ quality })
  }
  if (fileExtension == 'webp') {
    sharp.webp({ quality })
  }

  if (isWebp) {
    sharp.webp({ quality })
  }

  if (overlayImg) {
    sharp
      .flatten({ background: '#ff6600' })
      .composite([{ input: overlayImg, gravity }])
      .sharpen()
  }

  return sharp
}

// const resizeRemoteImage = ({ imageUrl, resizeOptions, fullWritePath }) =>
//   request()(imageUrl).then((imageStream) =>
//     imageStream
//       .pipe(getTransformer(resizeOptions))
//       .pipe(createWriteStream(fullWritePath))
//   )

// const resizeLocalImage = ({ imagePath, resizeOptions, fullWritePath }) =>
//   createReadStream(imagePath)
//     .pipe(getTransformer(resizeOptions))
//     .pipe(createWriteStream(fullWritePath))


const resize = (config, url) => {
  const { filePath = false, writeFileToDisk = false, host } = config

  const options = getOptions(url)
  const transform = getTransformer(options)

  const origin = path.join(filePath, options.fileName)
  const rstream = createReadStream(origin)

  rstream.pipe(transform)

  /**
   * TODO:Yazma işlemi buradan dışarı taşınmalı,
   * Mantık olarak burada sadece boyutlandırma yapıp stream döndürmeli
   */
  if (writeFileToDisk) {
    const target = path.join(filePath, options.folder, options.fileName)
    const wstream = createWriteStream(target)

    wstream.once('error', async (error) => {
      if (error.code === 'ENOENT') {
        await promises
          .mkdir(path.dirname(target), { recursive: true })
          .then(resize(config, url))
      }
    })

    transform.pipe(wstream)
  }

  return transform
}

/**
  * Return connection parameters used to connect to MySQL (obtained from the
  * DB_MYSQL_CONNECTION environment variable which should be a connection string in the format
  * "host=my.host.com; user=my-un; password=my-pw; database=my-db").
  *
  * @returns Object with host, user, password, database properties.
  */

const envKeyParser = (envKey) => {
  if (!process.env[envKey]) {
    throw new Error('No process.env.' + envKey + ' available')
  }

  const envKeyArray = process.env[envKey]
    ? process.env[envKey].split(';').map((v) => v.trim().split('='))
    : []

  const conf = envKeyArray.reduce((config, v) => {
    config[v[0].trim()] = v[1].trim()
    return config
  }, {})

  return conf
}

const setOptions = (opt) => {
  const conf = envKeyParser('SHARP_MIDDLEWARE')
  const options = {...conf, ...opt }

  options.filePath        = path.resolve(options.path)
  options.writeFileToDisk = options.writeFileToDisk == 'true' ? true : false

  return options
}


const expressMiddleware = (opt) => {

  const options = setOptions(opt)

  // middleware
  return (req, res) => {
    try {
      resize( { ...options }, req.url ).pipe(res)
    } catch (err) {
      res.send(err.message).status(500)
    }
  }
}


const koaMiddleware = (opt) => {

  const options = setOptions(opt)

  // middleware
  return (ctx) => {
    try {
      resize( { ...options }, ctx.url ).pipe(ctx.res)
    } catch (err) {
      ctx.res.send(err.message).status(500)
    }
  }
}

exports = module.exports = { expressMiddleware, koaMiddleware }
